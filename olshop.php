<?php
/*
Plugin Name: Checkout Marketplace
 */
 
function wpse_load_plugin_css() {
    $plugin_url = plugin_dir_url( __FILE__ );

    wp_enqueue_style( 'style', $plugin_url . 'css/style.css' );
}
add_action( 'wp_enqueue_scripts', 'wpse_load_plugin_css' );

Class Sibiz_Olshop
{
    public function __construct()
    {
        add_action( 'woocommerce_after_add_to_cart_button', array(&$this, 'content_after_addtocart_button') );
        add_filter( 'woocommerce_product_data_tabs', array(&$this,'custom_tab') );
        add_action( 'woocommerce_product_data_panels', array(&$this,'custom_tab_panel') );
        add_action( 'woocommerce_process_product_meta_simple', array(&$this,'save_linkolshop_option_fields')  );
        add_action( 'woocommerce_process_product_meta_variable', array(&$this,'save_linkolshop_option_fields')  );
    }

 
    function content_after_addtocart_button() {
        global $post;
        echo '
 
<br /><br />Silakan Beli via Online Shop Berikut:<br />
';  
 
        $tokopedia  = get_post_meta($post->ID,'link_tokopedia',true);
        $bukalapak  = get_post_meta($post->ID,'link_bukalapak',true);
        $shopee     = get_post_meta($post->ID,'link_shopee',true);
        $instagram  = get_post_meta($post->ID,'link_instagram',true);
        $whatsapp   = get_post_meta($post->ID,'link_whatsapp',true);
 
        if(!empty($tokopedia))
        {
            echo '<br /><a href="'.$tokopedia.'" class="btn btn-tokopedia"><img src="'.plugin_dir_url(__FILE__).'icon/tokopedia.png" class="img-icon"/> Klik untuk Beli di Tokopedia</a><br />
 
';
        }
 
        if(!empty($bukalapak))
        {
            echo '<br /><a href="'.$bukalapak.'" class="btn btn-bukalapak"><img src="'.plugin_dir_url(__FILE__).'icon/bukalapak.png" class="img-icon"/> Klik untuk Beli di Bukalapak</a><br />
 
';
        }
         
        if(!empty($shopee))
        {
            echo '<br /><a href="'.$shopee.'" class="btn btn-shopee"><img src="'.plugin_dir_url(__FILE__).'icon/shopee.png" class="img-icon"/> Klik untuk Beli di Shopee</a><br />
 
';
        }
 
        if(!empty($instagram))
        {
            echo '<br /><a href="'.$instagram.'" class="btn btn-instagram"><img src="'.plugin_dir_url(__FILE__).'icon/instagram.png" class="img-icon"/> Klik untuk Beli di Instagram</a><br />

';
        }
        
        if(!empty($whatsapp))
        {
            echo '<br /><a href="'.$whatsapp.'" class="btn btn-whatsapp"><img src="'.plugin_dir_url(__FILE__).'icon/whatsapp.png" class="img-icon"/> Hubungi kami melalui whatsapp</a><br />';

        }
 
        ;
    }
     
    function custom_tab( $tabs ) {
        $tabs['custom_tab'] = array(
            'label'  => __( 'Link Olshop', 'sibiz_link_olshop' ),
            'target' => 'link_olshop_panel',
            'class'  => array(),
        );
         
        return $tabs;
    }
 
    function custom_tab_panel() {
        ?>
         
<div id="link_olshop_panel" class="panel woocommerce_options_panel">
           
<div class="options_group">
            <?php woocommerce_wp_text_input( array( 'id' => 'link_tokopedia',
                        'label' => __( 'Link Tokopedia', 'textdomain' ),
                    )
                );
             
            woocommerce_wp_text_input( 
                array(
                        'id' => 'link_bukalapak',
                        'label' => __( 'Link Bukalapak', 'textdomain' ),
                    )
                );
             
            woocommerce_wp_text_input( 
            array(
                    'id' => 'link_shopee',
                    'label' => __( 'Link Shopee', 'textdomain' ),
                )
            );
 
            woocommerce_wp_text_input( 
                array(
                        'id' => 'link_instagram',
                        'label' => __( 'Link Instagram', 'textdomain' ),
                    )
                );
            woocommerce_wp_text_input( 
                array(
                        'id' => 'link_whatsapp',
                        'label' => __( 'Link Whatsapp', 'textdomain' ),
                    )
                );

            ?>
          </div>
 
        </div>
 
      <?php
    }
 
    function save_linkolshop_option_fields( $post_id ) {
         
        if ( isset( $_POST['link_tokopedia'] ) ) :
            update_post_meta( $post_id, 'link_tokopedia', $_POST['link_tokopedia'] );
        endif;
 
        if ( isset( $_POST['link_bukalapak'] ) ) :
            update_post_meta( $post_id, 'link_bukalapak', $_POST['link_bukalapak'] );
        endif;
 
        if ( isset( $_POST['link_shopee'] ) ) :
            update_post_meta( $post_id, 'link_shopee', $_POST['link_shopee'] );
        endif;
 
        if ( isset( $_POST['link_instagram'] ) ) :
            update_post_meta( $post_id, 'link_instagram', $_POST['link_instagram'] );
        endif;
        
        if ( isset( $_POST['link_whatsapp'] ) ) :
            update_post_meta( $post_id, 'link_whatsapp', $_POST['link_whatsapp'] );
        endif;
         
    }
     
}
 
$sibiz_olshop = new Sibiz_Olshop();